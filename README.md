# Wikipedia Featured Content Bot

Experimental bot for posting featured content from Wikipedia.

For now, this code supports:

* Posting "Today's featured article" from English Wikipedia to Mastodon

## Usage

The OAuth workflow isn't implemented yet. For now, just copy/paste the access token from your application in a `config/settings.toml` file:

```toml
access_token = "your_token"
```

## Help

See https://wikitech.wikimedia.org/wiki/Tool:Wikipedia_Featured_Content_Bot

## TODO

In absence of a phab project:

- [ ] Implement OAuth correctly 😅
- [ ] Tests
- [ ] Add arg parsing for loading config/settings.toml
- [ ] Logging
- [ ] Add pseudo-random scheduling for posts
- [ ] Proper error handling for feed loading, JSON parse errors, Mastodon errors, etc
- [ ] Post [the picture of the day](https://commons.wikimedia.org/wiki/Commons:Picture_of_the_day)
- [ ] Multiple language support

## Authors

- Kosta Harlan <kosta@fastmail.com>
