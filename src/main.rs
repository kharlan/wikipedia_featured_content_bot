use chrono::prelude::*;
use clap::Parser;
use config::Config;
use reqwest;
use reqwest::blocking::{multipart, Response};
use reqwest::header::{AUTHORIZATION, CONTENT_TYPE};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::process;
use tempfile::tempdir;

const USER_AGENT: &str = "Wikipedia Featured Content Bot/0.1.0 (https://wikitech.wikimedia.org/wiki/Tool:Wikipedia_Featured_Content_Bot; kosta@fastmail.com)";

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Whether to proceed with posting data.
    #[clap(short, long, value_parser)]
    dry_run: bool,
    /// Content to post
    #[clap(short, long, value_parser)]
    content_type: String,
}

#[derive(Deserialize, Debug)]
struct FeaturedContent {
    tfa: TodaysFeaturedArticle,
    image: PictureOfTheDay,
}

#[derive(Deserialize, Debug)]
struct PictureOfTheDay {
    title: String,
    file_page: String,
    description: DescriptionText,
    license: License,
    artist: Artist,
    credit: Credit,
    thumbnail: Thumbnail,
}

#[derive(Deserialize, Debug)]
struct DescriptionText {
    text: String,
}

#[derive(Deserialize, Debug)]
struct Thumbnail {
    source: String,
}

#[derive(Deserialize, Debug)]
struct Credit {
    text: String,
}

#[derive(Deserialize, Debug)]
struct License {
    r#type: String,
}

#[derive(Deserialize, Debug)]
struct Artist {
    text: String,
}

#[derive(Deserialize, Debug)]
struct TodaysFeaturedArticle {
    title: String,
    extract: String,
    normalizedtitle: String,
}

#[derive(Deserialize)]
struct MediaStatusResponse {
    id: String,
}

#[derive(Serialize)]
struct StatusPost {
    status: String,
    media_ids: Vec<String>,
    visibility: String,
    language: String,
    sensitive: bool,
}

fn main() {
    let args = Args::parse();
    let current_date: DateTime<Utc> = Utc::now();
    let url = format!(
        "https://api.wikimedia.org/feed/v1/wikipedia/en/featured/{year}/{month}/{day}",
        year = current_date.year(),
        month = current_date.format("%m"),
        day = current_date.format("%d")
    );
    println!("Querying featured content data: {}", url);
    let client = reqwest::blocking::Client::new();
    let response = client
        .get(url)
        .header("User-Agent", USER_AGENT)
        .send()
        .unwrap()
        .text()
        .unwrap();
    let featured_content: FeaturedContent =
        serde_json::from_str(&response).expect("Obtained a FeaturedContent object from JSON");
    let settings = Config::builder()
        .add_source(config::File::with_name("config/settings.toml"))
        .build()
        .unwrap()
        .try_deserialize::<HashMap<String, String>>()
        .unwrap();
    if args.content_type == "todays-featured-article" {
        post_todays_featured_article(client, settings, featured_content.tfa, args.dry_run);
    } else if args.content_type == "picture-of-the-day" {
        post_picture_of_the_day(client, settings, featured_content.image, args.dry_run);
    } else {
        eprintln!("Please supply a valid argument to --content-type.");
        process::exit(1);
    }
}

fn post_picture_of_the_day(
    client: reqwest::blocking::Client,
    settings: HashMap<String, String>,
    picture_of_the_day: PictureOfTheDay,
    dry_run: bool,
) {
    println!("{:#?}", picture_of_the_day);
    let image_src = picture_of_the_day.thumbnail.source;
    let tmp_dir = tempdir().unwrap();
    let file_path = tmp_dir.path().join(picture_of_the_day.title);
    let mut file = File::create(&file_path).unwrap();
    client
        .get(&image_src)
        .header("User-Agent", USER_AGENT)
        .send()
        .unwrap()
        .copy_to(&mut file)
        .expect("Copied file to local directory.");

    let description_text = picture_of_the_day.description.text.clone();
    let form = multipart::Form::new()
        .text("description", description_text)
        .file("file", &file_path);
    println!("Copied file to {:#?}", file_path);
    if dry_run {
        println!("Dry run, not posting!");
        return;
    }
    let response = client
        .post(format!(
            "https://{host}/{endpoint}",
            host = settings["host"],
            endpoint = "api/v1/media"
        ))
        .multipart(form.unwrap())
        .header(
            AUTHORIZATION,
            format!(
                "Bearer {access_token}",
                access_token = settings["access_token"]
            ),
        )
        .send()
        .unwrap()
        .text()
        .unwrap();
    println!("{:#?}", response);
    let response: MediaStatusResponse =
        serde_json::from_str(&response).expect("Constructed a MediaStatusResponse from JSON");
    let mut truncated_description = picture_of_the_day.description.text;
    if (truncated_description.len()) > 200 {
        truncated_description.truncate(200);
        truncated_description.push('…');
    }
    let status = format!(
        "Picture of the day: {description}\n\nArtist: {artist} | Credit: {credit} | License: {license} | {url}\n\n#Photography\n\n#Wikimedia",
        description = truncated_description,
        artist = picture_of_the_day.artist.text,
        credit = picture_of_the_day.credit.text,
        license = picture_of_the_day.license.r#type,
        url = picture_of_the_day.file_page
    );
    let image_post = StatusPost {
        media_ids: vec![response.id],
        status,
        sensitive: false,
        visibility: "public".to_string(),
        language: "en".to_string(),
    };
    post(
        client,
        settings,
        serde_json::to_string(&image_post).unwrap(),
        "api/v1/statuses".to_string(),
    );
    return;
}

fn post_todays_featured_article(
    client: reqwest::blocking::Client,
    settings: HashMap<String, String>,
    tfa: TodaysFeaturedArticle,
    dry_run: bool,
) {
    println!("{:#?}", tfa);
    let mut extract = tfa.extract;
    if (extract.len()) > 400 {
        extract.truncate(400);
        extract.push('…');
    }
    let post_status_content = format!(
        "Today's featured article: {title}\n\n{extract}\n\n{url}\n\n#Wikipedia",
        title = tfa.normalizedtitle,
        extract = extract,
        url = format!("https://en.wikipedia.org/wiki/{}", tfa.title)
    );
    println!("Post status content:");
    let mut request_json = HashMap::new();
    request_json.insert("status", post_status_content);
    request_json.insert("language", "en".to_string());
    if dry_run {
        println!("Dry run, not posting!");
        return;
    }

    let response = post(
        client,
        settings,
        serde_json::to_string(&request_json).unwrap(),
        "api/v1/statuses".to_string(),
    );
    println!("{:#?}", response.text().unwrap());
    return;
}

fn post(
    client: reqwest::blocking::Client,
    settings: HashMap<String, String>,
    request_json: String,
    endpoint: String,
) -> Response {
    println!("{}", request_json);
    let response = client
        .post(format!(
            "https://{host}/{endpoint}",
            host = settings["host"],
            endpoint = endpoint
        ))
        .header(CONTENT_TYPE, "application/json")
        .body(request_json)
        .header(
            AUTHORIZATION,
            format!(
                "Bearer {access_token}",
                access_token = settings["access_token"]
            ),
        )
        .send()
        .unwrap();
    if response.status().is_success() {
        println!("Successfully posted!");
    } else if response.status().is_server_error() {
        eprintln!("server error!");
        process::exit(1);
    } else {
        eprintln!("Something else happened. Status: {:?}", response.status());
        process::exit(1);
    }
    return response;
}
